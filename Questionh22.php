<!DOCTYPE html>
<html lang="en">
<head>
        
        
    <style>html {
        height: 100%;
        color: #ecf0f3;
      }
      .header{
    position:absolute;
    width: 450px;
    height: 500px;
    border-radius: 10px;
    left:50%;
    padding: 20px;
    box-sizing: border-box;
    background: #3b3208;
    font-family: 'Courier New', Courier, monospace;
    font-size: 17px;
    letter-spacing: 2px;
    color: rgb(255, 255, 255);
    border-radius: 10px;
    border-right-style: groove;
    border-right-color: #fff;
    border-bottom-style: groove;
    border-bottom-color:#fff;
    
  }
  .header2{
    position:absolute;
    width: 450px;
    height: 500px;
    border-radius: 10px;
    right:50%;
    top: 15%;
    padding: 20px;
    box-sizing: border-box;
    background:  #3b3208;
    font-family: 'Courier New', Courier, monospace;
    font-size: 17px;
    letter-spacing: 2px;
    color: rgb(255, 255, 255);
    border-radius: 10px;
    border-right-style: groove;
    border-right-color: #fff;
    border-bottom-style: groove;
    border-bottom-color:#fff;
    
  }
  .header3{
    position:absolute;
    width: 450px;
    height: 95px;
    border-radius: 10px;
    left:50%;
    top: 78%;
    padding: 20px;
    box-sizing: border-box;
    background:  #3b3208;
    font-family: 'Courier New', Courier, monospace;
    font-size: 17px;
    letter-spacing: 2px;
    color: rgb(255, 255, 255);
    border-radius: 10px;
    border-right-style: groove;
    border-right-color: #fff;
    border-bottom-style: groove;
    border-bottom-color:#fff;
    
  }
  .header4{
    position:absolute;
    width: 450px;
    height: 95px;
    border-radius: 10px;
    right:50%;
    bottom: 85%;
    padding: 10px;
    box-sizing: border-box;
    background:  #3b3208;
    font-family: 'Courier New', Courier, monospace;
    font-size: 17px;
    letter-spacing: 2px;
    color: rgb(255, 255, 255);
    border-radius: 20px;
    border-right-style: groove;
    border-right-color: #fff;
    border-bottom-style: groove;
    border-bottom-color:#fff;
    
  }
      body {
        margin:0;
        padding:0;
        font-family: sans-serif;
        background:#ecf0f3;
        color: #ecf0f3;
      }
      
      .container {
        position: absolute;
        top: 50%;
        left: 50%;
        width: 400px;
        font-family: 'Courier New', Courier, monospace;
        padding: 40px;
        transform: translate(-50%, -50%);
        /* background:linear-gradient(brown,#141e30); */
        box-sizing: border-box;
        box-shadow: 0 20px 25px rgba(0,0,0,.6);
        border-radius: 20px;
        color:#ecf0f3 ;
        border-top-style: outset;
        border-top-color:white;
        border-left-style:unset;
        border-left-color:  #3b3208;
        border-bottom-style: inset;
        border-bottom-color: #ecf0f3;
        margin-top: 3%;
      }
      .container .box {
        position: relative;
      }
      
      .container .box input {
        width: 100%;
        padding: 10px 0;
        font-size: 16px;
        color: #f8f8f8;
        margin-bottom: 30px;
        border: none;
        border-bottom: 1px solid #fff;
        outline: none;
        background: transparent;
      }
      .container .box label {
        position: absolute;
        top:0;
        left: 0;
        padding: 10px 0;
        font-size: 16px;
        color: #fff;
        pointer-events: none;
        transition: .5s;
      }
      
      .container .box input:focus ~ label,
      .container .box input:valid ~ label {
        top: -20px;
        left: 0;
        color: #f7f8fa;
        font-size: 12px;
      }
      
      .container form a {
        position: relative;
        display: inline-block;
        padding: 10px 90px;
        ;
        font-size: 16px;
        text-decoration: none;
        text-transform: uppercase;
        overflow: hidden;
        transition: .5s;
        margin-top: 40px;
        letter-spacing: 10px;
        background:   #3b3208;
        color: #fff;
      }
      
      .container a:hover {
        background:#ecf0f3;
        color: #02022a;
        border-radius: 5px;
        box-shadow: 0 0 5px ,#ecf0f3
                    0 0 25px #ecf0f3,
                    0 0 50px #ecf0f3,
                    0 0 100px #ecf0f3;
      }
      
  .container a span {
    position: absolute;
    display: block;
  }
  
  .container a span:nth-child(1) {
    top: 0;
    left: -100%;
    width: 100%;
    height: 2px;
    background: linear-gradient(90deg, transparent, #ecf0f3);
    animation: btn-anim1 1s linear infinite;
  }
  
  @keyframes btn-anim1 {
    0% {
      right: -100%;
    }
    50%,100% {
      left: 100%;
    }
  }
  
  .container a span:nth-child(2) {
    top: -100%;
    right: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(180deg, transparent, #ecf0f3);
    animation: btn-anim2 1s linear infinite;
    animation-delay: .25s
  }
  
  @keyframes btn-anim2 {
    0% {
      top: -100%;
    }
    50%,100% {
      top: 100%;
    }
  }
  
  .container a span:nth-child(3) {
    bottom: 0;
    right: -100%;
    width: 100%;
    height: 2px;
    background: linear-gradient(270deg, transparent, #ecf0f3);
    animation: btn-anim3 1s linear infinite;
    animation-delay: .5s
  }
  
  @keyframes btn-anim3 {
    0% {
      right: -100%;
    }
    50%,100% {
      right: 100%;
    }
  }
  
  .container a span:nth-child(4) {
    bottom: -100%;
    left: 0;
    width: 2px;
    height: 100%;
    background: linear-gradient(360deg, transparent, #ecf0f3);
    animation: btn-anim4 1s linear infinite;
    animation-delay: .75s
  }
  
  @keyframes btn-anim4 {
    0% {
      bottom: -100%;
    }
    50%,100% {
      bottom: 100%;
    }
  }
  .submit{
  border-style: hidden;
  background-color: transparent;
  font-size: large;
  font-style:unset;
  color: white;
  font-size: 16px;
  text-decoration: none;
  text-transform: uppercase;
  overflow: hidden;
  transition: .5s;
  margin-top: 10px;
  margin-bottom: 10px;
  letter-spacing:4px;
  
}
.submit:hover{
  color: #04083b;
}
      </style>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div class="header">
     <b> QUESTION 22:</b><br><br>
     22.	Write a C# Sharp program to find the eligibility of admission for a professional course based on the following criteria: <br>
i.	Jamb score >=180 <br>
ii.	Post utme >=170

    </div>
    <div class="header2"> </div>
    <div class="header3"></div>
    <div class="header4"></div>
    
        <div class="container">
            <form action="Question22.php" method="POST">
          
                    <div class="box">
                        Jamb Score:
                        <input type="text" name="js" required="">
                    </div>
               
              <a href="">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
              <input type="submit" value="Calculate" class="submit">
              </a>
            </form>
              </div> 
</body>
</html>
<?php
// if ($_POST['js']>180){
//   include 'Question22 1.php';
//};?>
